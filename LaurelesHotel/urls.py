
from django.contrib import admin
from django.urls import include, path
from Core.views import dashboard

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', dashboard, name="dashboard"),
    path('habitaciones/', include('Habitaciones.urls')),
    path('comprobantes/', include('Ventas.urls')),
]
