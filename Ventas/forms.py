from django import forms
from .models import Comprobante


class NewComprobanteForm(forms.Form):
    TIPO_COMPROBANTE = (
        ('B', 'Boleta'),
        ('F', 'Factura')
    )
    tipoComprobante = forms.ChoiceField(
        label="Tipo de Comprobante",
        choices=TIPO_COMPROBANTE,
        widget=forms.Select(),
        required=True
    )
