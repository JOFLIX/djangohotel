from django.urls import path
from .views import new_comprobante, list_comprobante, detail_comprobante

urlpatterns = [
    path('', list_comprobante, name="list_comprobante"),
    path('new/', new_comprobante, name="new_comprobante"),
    path('detail/<int:pk>/', detail_comprobante, name='detail_comprobante'),
]