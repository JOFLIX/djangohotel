from django.db import models


class Cliente(models.Model):
    nombre = models.CharField(max_length=255)
    documento = models.DecimalField(max_digits=11, decimal_places=0)
    celular = models.CharField(max_length=9)

    def __str__(self):
        return self.nombre