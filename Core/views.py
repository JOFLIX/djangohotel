from django.shortcuts import render


def dashboard(request):
    context = {}
    template_name = "Core/dashboard.html"
    return render(request, template_name, context)
