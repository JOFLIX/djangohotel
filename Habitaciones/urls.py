from django.urls import path
from .views import list_habitacions, new_habitacion, detail_habitacion

urlpatterns = [
    path('', list_habitacions, name="habitacion_list"),
    path('new/', new_habitacion, name="habitacion_new"),
    path('detail/<int:pk>/', detail_habitacion, name="habitacion_detail")
]