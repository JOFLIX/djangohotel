from django.shortcuts import render
from django.db.models import Q
from .models import Habitacion

carpeta_base = 'habitaciones/'


def list_habitacions(request):
    """
    Esta vista genera la lista de habitaciones
    :param request:
    :return:
    """
    piso = request.GET.get('q')
    tipo = request.GET.get('p')

    # Cosas del Filtro esto se puede mejorar usando Django Filter
    if piso and tipo:
        if piso == '0' and tipo == '0':
            query = Habitacion.objects.all().filter(visisble=True)
        elif piso == '0' and tipo != '0':
            query = Habitacion.objects.filter(tipo=tipo)
        elif piso != '0' and tipo == '0':
            query = Habitacion.objects.filter(piso=piso)
        else:
            query = Habitacion.objects.filter(
                Q(visisble__exact=True),
                Q(piso__exact=piso),
                Q(tipo__exact=tipo))
    else:
        query = Habitacion.objects.all().filter(visisble=True)

    context = {'habitaciones': query}
    template_name = carpeta_base + 'list_habitaciones.html'
    return render(request, template_name, context)


def detail_habitacion(request, pk):
    """
    Esta es la vista que genera el detalle de la habitacion
    TODO Crear los links para la creacion de reserva
    TODO Crear un historico de las reservas para una habitacions
    :param request:
    :param pk:
    :return:
    """
    q = Habitacion.objects.get(pk=pk)
    context = {'habitacion': q}
    template_name = carpeta_base + 'detail_habitaciones.html'
    return render(request, template_name, context)


def update_habitacion(request, pk):
    """
    Esta vista contrala la edicion de parametros de las Habitaciones
    TODO implementa la actualizacion de habitaciones
    :param request:
    :param pk:
    :return:
    """
    context = {}
    template_name = carpeta_base + 'update_habitacion.html'
    return render(request, template_name, context)



def new_habitacion(request):
    """
    Esta vista es la que maneja el formulario de creacion de habitaciones
    TODO implementar la creacion de habitaciones
    :param request:
    :return:
    """
    context = {}
    template_name = 'habitaciones/new_habitaciones.html'
    return render(request, template_name, context)
