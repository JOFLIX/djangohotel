"""
Modelo Habitacion:
Aqui se representa las habitaciones y los pisos
"""
from django.db import models


class TipoHabitacion(models.Model):
    """
    Clase Tipo
    - Estos pueden ser simples, dobles y matrimoniales
    - Puede existir
    """
    descripcion = models.CharField(max_length=255)

    def __str__(self):
        return self.descripcion


class Piso(models.Model):
    """
    Modelo Piso necesario para agregar mas pisos si esque es necesario
    """
    numero = models.DecimalField(max_digits=2, decimal_places=0)

    def __str__(self):
        return str(self.numero)


class Habitacion(models.Model):
    """
    Modelo Habitacion
    - Toda Habitacion pertenece a un piso
    - Las habitacion tiene estado que lo hace visible en caso de ser necesario sacarlo de sirculacion
    - Toda Habitacion tiene un tipo espesificado enla tabla TipoHabitacion
    """
    numero = models.DecimalField(max_digits=2, decimal_places=0)
    tipo = models.ForeignKey(TipoHabitacion, on_delete=models.CASCADE)
    piso = models.ForeignKey(Piso, on_delete=models.CASCADE)
    visisble = models.BooleanField(default=True)
    mantenimiento = models.BooleanField(default=False)

    def __str__(self):
        return "Habitacion " + str(self.numero) + " - Piso " + str(self.piso)

    def get_if_habitacion_is_ocupado(self):
        # TODO implementar una forma de saber si la habitacion esta ocupada
        # TODO implementar una forma de saber si al habitacion esta reservada
        return True

