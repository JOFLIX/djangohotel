from django.contrib import admin
from .models import  Habitacion, Piso, TipoHabitacion


admin.site.register(TipoHabitacion)

admin.site.register(Habitacion)

admin.site.register(Piso)
